#!/bin/bash
cd ~/p91 # pour crontab
dossier=945composants-STA	# 1/3 CIBLE (`mkdir` automatique)
orig=/public_html/SI1/	# 2/3 Source (dossier)
lesfic='945composants-STA.dot 945composants-STA.png'	# 3/3 liste de fichier(s)
pourvoir=~/public_html/p91/$dossier.html # ~~autogestion~~
lespseudo=p91liste-SI1.945 # liste confidentielle
log=$dossier/ramasser_$lespseudo.log # au format MarckDown pour pandoc
if [[ -d $dossier ]]
 then
 echo 'Le dossier "$dossier" existe.'
else
 if $(mkdir $dossier)
  then
  echo 'Le dossier "$dossier" à été créé.'
  else
  echo 'Échec mkdir $dossier !'
 fi
fi
# voir http://abs.traduc.org/abs-fr/ch14.html#readredir
exec 6>&1           # Lie le descripteur de fichier #6 avec stdout.
				 # Sauvegarde stdout.
exec > $log         # stdout remplacé par le fichier...
echo "% Évaluation de la livraison de"
echo " "
echo "~~~"
echo $lesfic
echo "~~~"
echo " dans \`~"$orig"\` le " ;  date
echo " "
while IFS=: read pseudo nomcomplet promo ignore
do
echo " " ; echo "* **$nomcomplet**"
for fic in $lesfic
	do
	if [[ -f /home/$promo/$pseudo$orig$fic ]]
	 then
		if $(cp -p /home/$promo/$pseudo$orig$fic $dossier/$pseudo$fic)
			then
			echo -e "\t* $fic livré"
			else
			echo -e "\t* $fic *NON COPIÉ*"
		fi
	 else
		echo -e "\t* insuffisant ($fic manquant)"
	fi
	done
done <$lespseudo   # Redirection d'entrées/sorties.
echo " "
echo "---- "
echo " "
echo "~~~"
ls -rtlh $dossier
echo "~~~"
exec 1>&6 6>&-      # Restaure stdout et ferme le descripteur de fichier #6.
pandoc --ascii -s -c /~fred/pandoc.css --highlight-style haddock -t html5 -o $pourvoir $log
exit 0
